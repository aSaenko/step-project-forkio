const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync');


const path = {
    build: {
        html: './build',
        css: './build/style/',
        js: './build/js',
        img: './build/img'
    },
    src:{
        html: './src/index.html',
        css: './src/scss/**/*.scss',
        js: './src/js/**/*.js',
        img: './src/img/**/*.*'
    },
    clean: './build/'
};
const htmlBuild = () => {
    return gulp
        .src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream());
};

const scssBuild = () => {
    return gulp.src(path.src.css)
        .pipe(sass())
        .pipe(concat("style.css"))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 100 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream());
};



const jsBuild = () => {
    return gulp.src(path.src.js)
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream());
};

const imgBuild = () => {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(browserSync.stream());
};


const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./build"    // что будет открывать browserSync!!!!
        }
    });
    gulp.watch('./src/**/*.scss', scssBuild);
     gulp.watch('./src/**/*.js', jsBuild);
     gulp.watch('./src/**/*.html', htmlBuild);

};
const cleanBuild = () => {
    return gulp.src(path.clean)
        .pipe(clean())
};


/*********TASK**************/
gulp.task('clean',cleanBuild);
gulp.task('gulp', htmlBuild);
gulp.task('scss', scssBuild );
gulp.task('js',jsBuild);
gulp.task('watcher',watcher);
gulp.task('img',imgBuild);


gulp.task('default', gulp.series(
    cleanBuild,
    htmlBuild,
    scssBuild,
    imgBuild,
    jsBuild,
    watcher
));












// const scssBuild = () => {
//     return gulp.src(path.src.css)
//         .pipe(sass().on('error', sass.logError))
//         .pipe(autoprefixer({
//             overrideBrowserslist: ['last 100 versions'],
//             cascade: false
//         }))
//         .pipe(gulp.dest(path.src.style));
// };
// const concatCss = () =>{
//     return gulp.src('./src/css/**/*.css')
//         .pipe(concat('style.scss'))
//         .pipe(gulp.dest(path.build.css));
//
//
//
//
// // const scssBuild = () => {
// //     return gulp.src('./src/scss/**/*.scss')
// //         .pipe(sass().on('error', sass.logError))
// //         .pipe(gulp.dest(path.src.style));
// // };
//
//
//
// const scssBuild = () => {
//     return gulp.src(path.src.scss)
//         .pipe(sass().on('error', sass.logError))
//         .pipe(autoprefixer({
//             overrideBrowserslist: ['last 100 versions'],
//             cascade: false
//         }))
//         .pipe(gulp.dest(path.build.css))
// };

// const gulp = require('gulp');
// const sass = require('gulp-sass');                          //sass
// const browserSync = require('browser-sync').create();       //runtime watcher and changer
// const clean = require('gulp-clean');                        //cleaner product directory "dev"
// const rename = require("gulp-rename");                        //rename files after minify
// const concat = require('gulp-concat');                      //concat for js
// // const autoprefixer = require('gulp-autoprefixer');          //cross-browser compatibility css
//
// function cleandev() {                                       //модуль отчистки папки перед каждой расспаковкой
//     return gulp.src('./build', {read: false})
//         .pipe(clean())
// }
//
// function buildhtml () {                                     //Copy index.html to dir "dev"
//     return gulp.src('./src/*.html')
//         .pipe(gulp.dest('./build/'))
//         .pipe(browserSync.stream());
// }
//
// // function scripts () {
// //     return gulp.src('src/**/*.js')
// //         .pipe(concat('script.js'))                                 //concat all js files
// //         .pipe(gulp.dest('./build/js'))
// //         .pipe(browserSync.stream());
// // }
//
// const jsBuild = () => {
//     return gulp.src(path.src.js)
//         .pipe(gulp.dest(path.build.js))
// };
//
// function forSass() {
//     return gulp.src('./src/scss/*.scss')
//         .pipe(sass())
//         // .pipe(autoprefixer({
//         //     browsers: ['> 0.1%'],                                // для браузеров которые использует 0.1%
//         //     cascade: false
//         // }))
//         .pipe(rename(function (path) {                          // function of rename extname for .css
//             path.extname = ".css";
//         }))
//         .pipe(gulp.dest('./build/style'))
//         .pipe(browserSync.stream());
// }
// function watch() {
//     browserSync.init({                                          // инструмент для live reload
//         server: {
//             baseDir: "./"
//         }
//     });
//     gulp.watch('./src/**/*.scss', forSass);             // ставим watcher для слежения за изменениями в файлах
//     gulp.watch('./src/**/*.js', jsBuild);
//     gulp.watch('./src/**/*.html', buildhtml);
// }
// gulp.task('cleandev', cleandev);
// gulp.task('buildHtml', buildhtml);
// gulp.task('scripts', jsBuild);
// gulp.task('sass', forSass);
// gulp.task('watch', watch);
// gulp.task('build', gulp.series('cleandev', gulp.series( buildhtml, jsBuild, forSass)));
// gulp.task('dev', gulp.series('build', watch));